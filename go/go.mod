module dependabot-gitlab/dependency-test

go 1.23

require github.com/urfave/cli/v2 v2.27.5
require gitlab.com/dependabot-gitlab/testing/packages/go v0.0.0-20250209040412-6d3f9109dff4

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.5 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20240521201337-686a1a2994c1 // indirect
)
